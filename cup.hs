cup :: t1 -> (t1 -> t2) -> t2
cup ml message = message ml

coffeeCup :: (Integer -> t2) -> t2
coffeeCup = cup 500

getMl :: ((a -> a) -> t) -> t
getMl aCup = aCup id

drink :: (Ord t1, Num t1) => ((a -> a) -> t1) -> t1 -> (t1 -> t2) -> t2
drink aCup mlDrank
  | mlDiff >= 0 = cup mlDiff
  | otherwise = cup 0
  where
    ml = getMl aCup
    mlDiff = ml - mlDrank

isEmpty :: (Eq a1, Num a1) => ((a2 -> a2) -> a1) -> Bool
isEmpty aCup = getMl aCup == 0
-- 