module Robots where

robot :: (a, b, c) -> ((a, b, c) -> t) -> t
robot (name, attack, hp) message = message (name, attack, hp)

killerRobot :: (([Char], Integer, Integer) -> t) -> t
killerRobot = robot ("killer", 25, 200)

gentleGiant :: (([Char], Integer, Integer) -> t) -> t
gentleGiant = robot ("gentleGiant", 10, 300)

name :: (a, b, c) -> a
name (n, _, _) = n
attack :: (a, b, c) -> b
attack (_, a, _) = a
hp :: (a, b, c) -> c
hp (_, _, hp) = hp

getName :: (((a, b, c) -> a) -> t) -> t
getName aRobot = aRobot name

getAttack :: (((a, b, c) -> b) -> t) -> t
getAttack aRobot = aRobot attack

getHP :: (((a, b, c) -> c) -> t) -> t
getHP aRobot = aRobot hp

setName :: (((a1, b, c) -> ((a2, b, c) -> t1) -> t1) -> t2) -> a2 -> t2
setName aRobot newName = aRobot (\(n, a, h) ->
  robot (newName, a, h))

setAttack :: (((a, b1, c) -> ((a, b2, c) -> t1) -> t1) -> t2) -> b2 -> t2
setAttack aRobot newAttack = aRobot (\(n, a, h) ->
  robot (n, newAttack, h))

setHP :: (((a, b, c1) -> ((a, b, c2) -> t1) -> t1) -> t2) -> c2 -> t2
setHP aRobot newHP = aRobot (\(n, a, h) ->
  robot (n, a, newHP))

printRobot :: (Show a1, Show a2) => ((([Char], a1, a2) -> [Char]) -> t) -> t
printRobot aRobot = aRobot (\(n, a, h) ->
                              n ++ ", attack:" ++ show a ++ ", health:" ++ show h)

damage :: Num c => (((a, b, c) -> ((a, b, c) -> t1) -> t1) -> t2) -> c -> t2
damage aRobot attackDamage = 
  aRobot (\(n, a, h) -> robot (n, a, h - attackDamage))

fight :: (Ord a1, Num a1) => (((a2, b1, b1) -> b1) -> a1)
                            -> (((a3, b2, a1) -> ((a3, b2, a1) -> t1) -> t1) -> t2) -> t2
fight aRobot defender = damage defender attack
  where attack = if getHP aRobot > 10 then getAttack aRobot else 0


gentleGiantRound1 = fight killerRobot gentleGiant

killerRobotRound1 = fight gentleGiant killerRobot

gentleGiantRound2 = fight killerRobotRound1 gentleGiantRound1

killerRobotRound2 = fight gentleGiantRound1 killerRobotRound1

gentleGiantRound3 = fight killerRobotRound2 gentleGiantRound2

killerRobotRound3 = fight gentleGiantRound2 killerRobotRound2

robotsLifes :: [Integer]
robotsLifes = map getHP [gentleGiant, killerRobot]

-- threeRoundFight robotA robotB = map printRobot [robotA, robotB]
  -- where
  --   rb1 = fight robotA robotB
  --   ra1 = fight robotB robotA
  --   rb2 = fight ra1 rb1
  --   ra2 = figth rb1 ra1
  --   rb3 = fight ra2 rb2
  --   ra3 = fight rb2 ra2